import copy
import time
import csv
import json
import datetime
from slugify import slugify
import requests
import urllib.request

from urllib.request import urlopen
from parsel import Selector

baseURL='http://localhost:1337/api/v1/'
header={
    'authorization':'Bearer  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFiY0BnbWFpbC5jb20iLCJpYXQiOjE1NjU1MjA1MDIsImV4cCI6MTU5NjYyNDUwMn0.saFUK2V9qt9vdRaiXbliy5scmqQGWWsBoGexB5cxd8M'
}

def get_template_line():
     # Read data that needs processing from CSV files
    template = []
    with open('categories.csv.csv', newline='', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            template.append(row)

    return template

datas = get_template_line()
arr=[]
for data in datas:
    data1={
        "name":data['name'],
        "content": data['content'],
        "image":'images/'+data['image'],
    }
    if(data['parent']):
        data1['parent']=arr[int(data['parent'])-1]["payload"]['_id']
    
    
    r = requests.post(url = baseURL+'categories',data=data1, headers = header)
    arr.append(r.json())
    # print(r.json())


print(arr[0]["payload"]['_id'])




                
